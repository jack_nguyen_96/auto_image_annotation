import numpy as np 
import cv2
import os
import time

import utils



'''
Checking all files in {root_folder}, if file is image and have
the different to {blank_image_template} less than {threshold},
remove image, or else resize image to (224,224)
'''
def repair_dataset(root_folder, blank_image_template, threshold=0.2):
    list_files = []
    list_files = utils.get_all_files_in_folder(root_folder, list_files)

    image = cv2.imread(blank_image_template)
    image = cv2.resize(image, (224,224))
    template_image = np.copy(image)


    for file_name in list_files:
        if utils.check_extention(file_name, ['.jpg', '.JPG']):
            ## if the different between current image and template image
            ## is less than threshold, delete current image
            if __cal_images_diff(file_name, template_image) < threshold:
                os.remove(file_name)
                print(file_name, ': deleted')
            ## or else resize image to (224,224)
            else:
                img = cv2.imread(file_name)
                img = cv2.resize(img, (224, 224))
                cv2.imwrite(file_name, img)
                print(file_name, ': resized')

'''
Calculate average of different pixels between two image
@@ params {path1}, {path2}: located of images
'''
def __cal_images_diff(path1, template_image):
    print (path1)
    try:
        img1 = cv2.imread(path1)
        img1 = cv2.resize(img1, (224,224))

        diff = np.average(img1 != template_image)
        return diff
    except:
        return 0.0

if __name__ == '__main__':
    start = time.time()
    repair_dataset('C:\\ImageData\\Flickr', 'C:\\ImageData\\Flickr\\zoos\\0202_480119727.jpg', threshold=0.2)
    end   = time.time()
    print('Done, time= ', end-start)