import os

'''
Get all file in folder {folder_path} and also in sub-folders
@@ params {list_files}: where to save file paths
@@ return file paths
'''
def get_all_files_in_folder(folder_path, list_files):
    for f in os.listdir(folder_path):
        file_path = folder_path + "/" + f
        if os.path.isdir(file_path):
            list_files = get_all_files_in_folder(file_path, list_files)
        elif os.path.isfile(file_path):
            list_files.append(file_path)
    return list_files

'''
Checking if {file_name} have an extention in {list_extention}
{list_extention} must include '.', example '.jpg'
'''
def check_extention(file_name, list_extention):
    if os.path.splitext(file_name)[1] in list_extention:
        return True
    return False
