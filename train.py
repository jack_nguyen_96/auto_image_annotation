import tensorflow as tf 
from keras import backend as K 
import time

import get_dataset
import custom_resnet


def custom_loss(y_true, y_pred):
    return K.mean(K.square((K.round(y_true) - K.round(y_pred))))

## Load data
train_X, train_Y, test_X, test_Y = get_dataset.get_dataset('./ImageData', './NUS-WIDE-SCENE')

model = custom_resnet.CustomResNet50(input_shape=(224,224,3))
model.compile(optimizer='Adam', loss='binary_crossentropy')
model.summary()

print ('Training')
start = time.time()
model.fit(train_X, train_Y, batch_size=10, epochs=1, validation_split=0.2)
end   = time.time()

model.save_weights('weights.h5')
print('Done, time= ', end-start)
