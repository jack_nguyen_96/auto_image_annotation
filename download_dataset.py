from urllib import request
import os
import time

import get_dataset

'''
Download NUS-WIDE dataset
@@ param {NUS_WIDE_urls_path}: NUS-WIDE-urls folder
'''
def download(NUS_WIDE_urls_path, list_files):
    train_file_names = list_files[0]
    for i in range(len(train_file_names)):
        train_file_names[i] = __get_photo_id_from_name(train_file_names[i])

    test_file_names  = list_files[1]
    for i in range(len(test_file_names)):
        test_file_names[i] = __get_photo_id_from_name(test_file_names[i])

    f = open(NUS_WIDE_urls_path + './NUS-WIDE-urls.txt', 'r')
    ## read header
    k = 0 
    s = f.readline()
    while s != '':
        s = f.readline()
        if s == '':
            break

        params = s.split()
        for i in range(2,6):
            # create save folder
            ## params[0] is where file saved
            ## params[1] is photo id
            ## params[2] -> params[5] is url of photo
            if params[1] in train_file_names or params[1] in test_file_names:
                if os.path.exists(params[0]):
                    k += 1
                    print(k, params[0])
                    continue 
                save_folder = __get_parent_folder(params[0])
                if not os.path.exists(save_folder):
                    os.makedirs(save_folder)
                # download image and save
                file_name = __download_and_save(params[i], params[0])
                if file_name is not None:
                    k += 1
                    print(k, params[0])
                    break

'''
Try to download image from url {file_url}
and save this image to {save_path} location
If {file_url} is not available, return None
'''
def __download_and_save(file_url, save_path):
    if file_url != 'null':
        try:
            file_name, header = request.urlretrieve(file_url, save_path)
            return file_name
        except:
            print('Error at file: ', file_url)
            return None
    return None

'''
Get parent folder of {file_path}
Example: 'root/parent/file.txt' ---> return: 'root/parent'
'''
def __get_parent_folder(file_path):
    path    = str(file_path)
    for i in range(len(file_path)-1, -1, -1):
        if path[i] != '\\':
            path = path[:i] + path[(i+1):]
        else:
            path = path[:i] + path[(i+1):]
            break
    return path

def __get_photo_id_from_name(file_name):
    photo_id = ''
    start = False
    for c in file_name:
        if c == '_':
            start = True
        elif c == '.':
            break
        elif start == True:
            photo_id = photo_id + c  
    return photo_id

if __name__ == '__main__':
    print('Downloading dataset')
    start = time.time()
    ## Get list of images file
    image_names = get_dataset.get_images_lists('./NUS-WIDE-SCENE')
    labels       = get_dataset.get_labels_list('./NUS-WIDE-SCENE')
    download('./NUS-WIDE-urls', image_names)
    end   = time.time()
    print('Done, time= ', end-start)