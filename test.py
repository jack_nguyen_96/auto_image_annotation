import numpy as np
import cv2
import sys
import os

import custom_resnet


'''
Predict an image
@@ params {weight_path}: path trained weight file
@@ params {img_paths}: list of image path
'''
def run(weight_path, img_paths):
    ## checking weight_path is exist
    if not os.path.exists(weight_path):
        print ("Could not find out ", weight_path)
        exit()

    ## create model
    model = custom_resnet.CustomResNet50(input_shape=(224,224,3))
    model.load_weights(weight_path)

    ## predict
    for img_path in img_paths:
        img, res     = predict(model, img_path)
        res          = decoding(res)
        dst          = print_result(img, res)
        cv2.imshow(img_path, dst)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def predict(model, img_path):
    try:
        img     = cv2.imread(img_path)
    except:
        print ("Could not open image")
        return -1

    img_    = cv2.resize(img, (224,224))
    img_    = np.expand_dims(img_, axis=0)
    res     = model.predict(img_)
    return img, res

def decoding(result):
    tags    = ['airport', 'beach', 'bridge', 'buildings', 'castle', 'cityscape',
            'clouds', 'frost', 'garden', 'glacier', 'grass', 'harbor',
            'house', 'lake', 'moon', 'mountain', 'nighttime', 'ocean', 'plants',
            'railroad', 'rainbow', 'reflection', 'road', 'sky', 'snow', 'street',
            'sunset', 'temple', 'town', 'valley', 'water', 'waterfall', 'window']
    res     = np.reshape(result, len(tags))
    arg     = np.argsort(res)

    res     = res[arg][::-1]
    tags_   = np.asarray(tags)[arg][::-1]

    res     = np.asarray([(res[i], tags_[i]) for i in range(len(tags_))])
    return res

def print_result(img, result):
    text    = ''
    dst     = np.copy(img)

    for i in range(5):
        text    = result[i][1] + ': ' + result[i][0]
        dst     = cv2.putText(dst, text, (50,i*25+25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 2)
    return dst


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Missing arguments')
        print('Following this patten: test.py <weight_path> <img1_path> <img2_path> <img3_path>...')
        exit()
    
    weight_path = sys.argv[1]
    img_paths   = sys.argv[2:]
    run(weight_path, img_paths)


