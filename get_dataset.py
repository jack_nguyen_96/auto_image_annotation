import cv2
import numpy as np
import os
import time

def get_dataset(ImageData_path, NUS_WIDE_SCENE_folder_path):
    print("Loading dataset")
    start = time.time()

    ## load origin dataset
    train_file_names, test_file_names = __get_X(NUS_WIDE_SCENE_folder_path)
    train_labels, test_labels         = __get_Y(NUS_WIDE_SCENE_folder_path)
    ## remove out of date images
    train_X = []
    train_Y = []
    test_X  = []
    test_Y  = []

    for i in range(len(train_file_names)):
        full_file_path = ImageData_path + '/Flickr/' + train_file_names[i]
        if not os.path.exists(full_file_path):
            continue 
        try:
            print('Loading: ', full_file_path)
            img = cv2.imread(full_file_path)
            train_X.append(img)
            train_Y.append(train_labels[i])
        except:
            pass
    train_X = np.asarray(train_X)
    train_Y = np.asarray(train_Y)
    print(train_X.shape)
    print(train_Y.shape)
    train_Y = train_Y.reshape((train_Y.shape[0], 1, 1, train_Y.shape[1]))

    for i in range(len(test_file_names)):
        full_file_path = ImageData_path + '/Flickr/' + test_file_names[i]
        if not os.path.exists(full_file_path):
            continue 
        try:
            print('Loading: ', full_file_path)
            img = cv2.imread(full_file_path)
            test_X.append(img)
            test_Y.append(train_labels[i])
        except:
            pass
    test_X = np.asarray(test_X)
    test_Y = np.asarray(test_Y)
    print(test_X.shape)
    print(test_Y.shape)

    ## in case of no test sample, split 80% of train samples as test samples
    if test_X.shape[0] == 0:
        train_length = train_X.shape[0]
        split_pos   = int(train_length*0.8)
        test_X = train_X[split_pos:train_length]
        test_Y = train_Y[split_pos:train_length]
        train_X = train_X[0:split_pos]
        train_Y = train_Y[0:split_pos]
    else:
        test_Y = test_Y.reshape((test_Y.shape[0], 1, 1, test_Y.shape[1]))

    end = time.time()
    print("Done, time= ", end-start)

    return train_X, train_Y, test_X, test_Y
    

def __get_X(NUS_WIDE_SCENE_folder_path):
    test_file           = NUS_WIDE_SCENE_folder_path + '/NUS-WIDE-SCENE/image list/Test_imageOutPutFileList.txt'
    test_file_names     = __get_image_list(test_file)
    train_file          = NUS_WIDE_SCENE_folder_path + '/NUS-WIDE-SCENE/image list/Train_imageOutPutFileList.txt'
    train_file_names    = __get_image_list(train_file)
    return train_file_names, test_file_names

def __get_Y(NUS_WIDE_SCENE_folder_path):
    train_labels    = __get_labels_list(NUS_WIDE_SCENE_folder_path, train_labels=True)
    test_labels     = __get_labels_list(NUS_WIDE_SCENE_folder_path, train_labels=False)
    return train_labels, test_labels

def __get_labels_list(NUS_WIDE_SCENE_folder_path, train_labels=True):
    tags = ['airport', 'beach', 'bridge', 'buildings', 'castle', 'cityscape',
            'clouds', 'frost', 'garden', 'glacier', 'grass', 'harbor',
            'house', 'lake', 'moon', 'mountain', 'nighttime', 'ocean', 'plants',
            'railroad', 'rainbow', 'reflection', 'road', 'sky', 'snow', 'street',
            'sunset', 'temple', 'town', 'valley', 'water', 'waterfall', 'window']

    labels_list = []
    
    for tag in tags:
        if train_labels == True:
            file_name = NUS_WIDE_SCENE_folder_path + '/NUS-WIDE-SCENE/ground truth/Train_Labels_' + tag + '.txt'
        else:
            file_name = NUS_WIDE_SCENE_folder_path + '/NUS-WIDE-SCENE/ground truth/Test_Labels_' + tag + '.txt'
        labels = __get_labels(file_name)
        labels_list.append(labels)

    labels_list = np.asarray(labels_list)
    labels_list = np.transpose(labels_list)

    return labels_list

    

def __get_image_list(file_path):
    file_names = []

    f = open(file_path, 'r')
    print ("Reading image list: ", file_path)
    s = 'something'
    while s != '':
        s = f.readline()
        if s != '':
            ## restrip will remove '\n' of string
            file_names.append(s.rstrip())
    f.close()

    print ("Done, ", len(file_names), ' files')
    return file_names

def __get_labels(file_path):
    labels = []

    f = open(file_path, 'r')
    print ("Reading label list: ", file_path)
    s = 'something'
    while s != '':
        s = f.readline()
        if s != '':
            label = int(s)
            labels.append(label)
        else:
            break
    f.close()

    print ("Done, ", len(labels), ' labels')
    return labels

if __name__ == '__main__':
    train_X, train_Y, test_X, test_Y = get_dataset('C:\\ImageData', './NUS-WIDE-SCENE')
    print (train_X.shape)
    print (train_Y.shape)
    print (test_X.shape)
    print (test_Y.shape)